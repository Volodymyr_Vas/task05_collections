package com.vov4ik.controller;

import com.vov4ik.model.EnumMenu;
import com.vov4ik.model.MapMenu;

public class MenuController {
    EnumMenu menu = new EnumMenu();
    MapMenu mapMenu = new MapMenu();
    public void showEnumMenu() {
        menu.makeEnumMenu();
        menu.checkInput();
    }

    public void showMapMenu() {
        mapMenu.showMenu();
        mapMenu.showSelectedItem();
    }
}
