package com.vov4ik;

import com.vov4ik.view.MenuView;

public class App {
    public static void main(String[] args) {
        MenuView menuView = new MenuView();
        menuView.start();
    }
}
