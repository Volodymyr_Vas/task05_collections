package com.vov4ik.model;

public enum UserMenu {
    ListOfAppliances("1"),
    AppliancesCharacteristics("2"),
    SummaryPower("3"),
    Quit("Q");

    private String menuItemNumber;

    UserMenu(String menuItemNumber) {
        this.menuItemNumber = menuItemNumber;
    }

    public String getMenuItemNumber() {
        return menuItemNumber;
    }
}
