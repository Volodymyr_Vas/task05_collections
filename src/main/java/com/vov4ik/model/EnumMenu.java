package com.vov4ik.model;

import com.vov4ik.logger.MyLogger;
import com.vov4ik.model.appliances.AppliancesNames;
import com.vov4ik.model.appliances.Iron;
import com.vov4ik.model.appliances.TV;
import com.vov4ik.model.appliances.WaterHeater;

import java.util.Scanner;

import static com.vov4ik.model.UserMenu.*;

public class EnumMenu {
    Iron iron = new Iron();
    WaterHeater waterHeater = new WaterHeater();
    TV tv = new TV();
    private MyLogger myLogger = new MyLogger();
    Scanner scanner = new Scanner(System.in);
    String input;

    public void makeEnumMenu() {
        myLogger.printInfo(ListOfAppliances.getMenuItemNumber()
                + ". " + ListOfAppliances.name());
        myLogger.printInfo(AppliancesCharacteristics.getMenuItemNumber()
                + ". " + AppliancesCharacteristics.name());
        myLogger.printInfo(SummaryPower.getMenuItemNumber()
                + ". " + SummaryPower.name());
        myLogger.printInfo(Quit.getMenuItemNumber()
                + ". " + Quit.name());
    }

    public void checkInput(){
        do {
            input = scanner.nextLine().toUpperCase();
            if (input.equals(ListOfAppliances.getMenuItemNumber())) {
                myLogger.printInfo("List of Appliances:");
                for (AppliancesNames appliancesNames : AppliancesNames.values()) {
                    myLogger.printInfo(appliancesNames.name());
                }
                myLogger.printInfo("\nMenu:");
                makeEnumMenu();
            }else if (input.equals(AppliancesCharacteristics.getMenuItemNumber())) {
                myLogger.printInfo("Characteristics:");
                myLogger.printInfo(iron.toString());
                myLogger.printInfo(waterHeater.toString());
                myLogger.printInfo(tv.toString());
                myLogger.printInfo("\nMenu:");
                makeEnumMenu();
            }else if (input.equals(SummaryPower.getMenuItemNumber())) {
                int summaryPower = iron.getPower()
                        + waterHeater.getPower()
                        + tv.getPower();
                myLogger.printInfo("Summary power of all devices is: " + summaryPower + " Watt");
                myLogger.printInfo("\nMenu:");
                makeEnumMenu();
            }else if (input.equals(Quit.getMenuItemNumber())) {
                myLogger.printInfo("Have a nice day");
            }
            else {
                myLogger.printWarning("Input was incorrect. Please, try again");
            }
        } while (!(input.equals(Quit.getMenuItemNumber())));
    }
}
