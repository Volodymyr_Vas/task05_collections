package com.vov4ik.model;

import com.vov4ik.logger.MyLogger;
import com.vov4ik.model.appliances.AppliancesNames;
import com.vov4ik.model.appliances.Iron;
import com.vov4ik.model.appliances.TV;
import com.vov4ik.model.appliances.WaterHeater;
import com.vov4ik.view.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapMenu {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    Iron iron;
    WaterHeater waterHeater;
    TV tv;
    private MyLogger myLogger;
    Scanner scanner;
    String input;

    public MapMenu() {
        iron = new Iron();
        waterHeater = new WaterHeater();
        tv = new TV();
        myLogger = new MyLogger();
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - ListOfAppliances");
        menu.put("2", "2 - AppliancesCharacteristics");
        menu.put("3", "3 - SummaryPower");
        menu.put("Q", "Q - Quit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showListOfAppliances);
        methodsMenu.put("2", this::showAppliancesCharacteristics);
        methodsMenu.put("3", this::showSummaryPower);
        methodsMenu.put("Q", this::quit);
    }

    private void showListOfAppliances() {
        myLogger.printInfo("List of Appliances:");
        for (AppliancesNames appliancesNames : AppliancesNames.values()) {
            myLogger.printInfo(appliancesNames.name());
        }
    }

    private void showAppliancesCharacteristics() {
        myLogger.printInfo("Characteristics:");
        myLogger.printInfo(iron.toString());
        myLogger.printInfo(waterHeater.toString());
        myLogger.printInfo(tv.toString());
    }

    private void showSummaryPower() {
        int summaryPower = iron.getPower()
                + waterHeater.getPower()
                + tv.getPower();
        myLogger.printInfo("Summary power of all devices is: " + summaryPower + " Watt");
    }

    private void quit() {
        myLogger.printInfo("Have a nice day");
    }

    public void showMenu(){
        for (String menuItem : menu.values()) {
            myLogger.printInfo(menuItem);
        }
    }

    public void showSelectedItem() {
        do {
            input = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(input).print();
            } catch (Exception ex) {
                myLogger.printWarning("Input was incorrect. Please, try again");
            }
        } while (!input.equals("Q"));
    }
}
