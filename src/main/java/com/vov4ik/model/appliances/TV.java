package com.vov4ik.model.appliances;

public class TV {
    private int power = 400;
    private boolean switchedOn = false;
    private boolean pluggedIntoSocket = true;
    private boolean broken = false;

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "TV{" +
                "power=" + power +
                ", switchedOn=" + switchedOn +
                ", pluggedIntoSocket=" + pluggedIntoSocket +
                ", broken=" + broken +
                '}';
    }
}
