package com.vov4ik.model.appliances;

public class Iron {

    private int power = 2300;
    private boolean switchedOn = false;
    private boolean pluggedIntoSocket = true;
    private boolean broken = false;

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "Iron{" +
                "power=" + power +
                ", switchedOn=" + switchedOn +
                ", pluggedIntoSocket=" + pluggedIntoSocket +
                ", broken=" + broken +
                '}';
    }
}
