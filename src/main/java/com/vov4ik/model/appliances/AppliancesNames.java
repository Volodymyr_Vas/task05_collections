package com.vov4ik.model.appliances;

public enum AppliancesNames {
    Iron,
    TV,
    WaterHeater
}
