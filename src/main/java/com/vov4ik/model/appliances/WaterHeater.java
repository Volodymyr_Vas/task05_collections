package com.vov4ik.model.appliances;

public class WaterHeater {
    private int power = 1800;
    private boolean switchedOn = true;
    private boolean pluggedIntoSocket = true;
    private boolean broken = false;

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "WaterHeater{" +
                "power=" + power +
                ", switchedOn=" + switchedOn +
                ", pluggedIntoSocket=" + pluggedIntoSocket +
                ", broken=" + broken +
                '}';
    }
}
