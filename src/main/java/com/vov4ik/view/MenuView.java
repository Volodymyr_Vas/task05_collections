package com.vov4ik.view;

import com.vov4ik.controller.MenuController;
import com.vov4ik.logger.MyLogger;

public class MenuView {
    public void start() {
        MyLogger logger = new MyLogger();
        logger.printInfo("Welcome to our app. Choose one of the menu items, please:\n");
        MenuController menuController = new MenuController();
        //menuController.showEnumMenu();
        menuController.showMapMenu();
    }
}
