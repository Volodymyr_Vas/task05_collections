package com.vov4ik.view;

@FunctionalInterface
public interface Printable {

  void print();
}
